shotnum=58963;
shotnum=57218;
% shotnum=53362;
dir_simulations = '/home/matval/WORK/tr_client/';
plot_totalneutrons=0; %0 is false!
plot_neutronsbysource=0;
plot_densities=0;
plot_comparison_singlebeams=0;
plot_initialconditions=0;
plot_beta=1;
plot_losses=0;
convertfig=0;
switch shotnum
    case 58963
        ids_1beam = {'V01', 'V04', 'V05', 'V06'};
        ids_2beam = {'V10', 'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17'};
        ids_1beam50 = {'V30', 'V31', 'V32', 'V33'};
    case 57218
        ids_1beam = {'V07', 'V24', 'V25', 'V26'};
        ids_2beam = {'V18', 'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17'};
        ids_1beam50 = {'V30', 'V31', 'V32', 'V33'};    
    case 53362
        ids_1beam = {'V21', 'V04', 'V05', 'V06'};
        ids_2beam = {'V10', 'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17'}; 
        ids_1beam50 = {'V30', 'V31', 'V32', 'V33'};
end
target_1beam = {'D', 'D', 'H', 'H'};
source_1beam = {'D', 'H', 'H', 'D'};
target_2beam = {'D', 'D', 'H', 'H', 'D', 'H', 'D', 'H'};
source_2beam = {'DD', 'HH', 'HH', 'DD', 'HD', 'HD', 'DH', 'DH'};
target_1beam50 = {'D', 'D', 'H', 'H'};
source_1beam50 = {'D@50', 'H@50', 'H@50', 'D@50'};
%% plot total amount of neutrons
if plot_totalneutrons
    %% initialize figures
    fh=figure(); haxH=axes; hold(haxH, 'on');
    fd=figure(); haxD=axes; hold(haxD, 'on');

    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        neut = sim.allvars.NEUTT.data;
        if target_1beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-';
        else
            lc='b-';
        end
        semilogy(hax, time, neut, lc, 'DisplayName', sprintf('%s', source_1beam{id}));
    end

    %% 2 beams
    for id=1:numel(ids_2beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_2beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        neut = sim.allvars.NEUTT.data;
        if target_2beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_2beam{id}(1) == 'D'
            lc='r';
        else
            lc='b';
        end
        if source_2beam{id}(2)=='H'
            lc = [lc, ':'];
        else
            lc = [lc, '--'];
        end
        semilogy(hax, time, neut, lc, 'DisplayName', sprintf('%s', source_2beam{id}));
    end

    %% fancy graphs
    for h=[haxH, haxD]
        hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
        xlabel(h, ['time (s)']); ylabel(h, ['Neutrons (1/s)'])
        set(h, 'Yscale','log');
    end
    title(haxH, sprintf('%d H plasma', shotnum))
    title(haxD, sprintf('%d D plasma', shotnum))
    savefig(fh, sprintf('%s%d/%d_Htarget', dir_simulations, shotnum, shotnum))
    savefig(fd, sprintf('%s%d/%d_Dtarget', dir_simulations, shotnum, shotnum))
end

%% plot sources of neutrons
if plot_neutronsbysource
    %% initialize figures
%     fh=figure(); haxH=axes; hold(haxH, 'on');
%     fd=figure(); haxD=axes; hold(haxD, 'on');
    times=[0.85 0.95];
    data_source = struct();
    data_source.Y = struct();
    data_source.Y.H = ones(8,3); data_source.label.H = {};
    data_source.Y.D = ones(8,3); data_source.label.D = {};
    ind_shot_plot_H = 1;
    ind_shot_plot_D = 1;

    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        ind = time<times(2) & time>times(1);
        ts=target_1beam{id};
        ss = source_1beam{id};
        bbneut = sim.allvars.BBNTS.data; bbneut = mean(bbneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 1) = bbneut;', ts, ts));
        btneut = sim.allvars.BTNTS.data; btneut = mean(btneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 2) = btneut;', ts, ts));
        ttneut = sim.allvars.NEUTX.data; ttneut = mean(ttneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 3) = ttneut;', ts, ts));
        eval(sprintf('data_source.label.%s{ind_shot_plot_%s} = ''%s'';', ts, ts, ss));
        eval(sprintf('ind_shot_plot_%s = ind_shot_plot_%s+1;', ts,ts));
    end

    %% 2 beam
    for id=1:numel(ids_2beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_2beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        ind = time<times(2) & time>times(1);
        ts=target_2beam{id};
        ss = source_2beam{id};
        bbneut = sim.allvars.BBNTS.data; bbneut = mean(bbneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 1) = bbneut;', ts, ts));
        btneut = sim.allvars.BTNTS.data; btneut = mean(btneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 2) = btneut;', ts, ts));
        ttneut = sim.allvars.NEUTX.data; ttneut = mean(ttneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 3) = ttneut;', ts, ts));
        eval(sprintf('data_source.label.%s{ind_shot_plot_%s} = ''%s'';', ts, ts, ss));
        eval(sprintf('ind_shot_plot_%s = ind_shot_plot_%s+1;', ts,ts));
    end
    %% 1 beam@50
    for id=1:numel(ids_1beam50)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam50{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        ind = time<times(2) & time>times(1);
        ts=target_1beam50{id};
        ss = source_1beam50{id};
        bbneut = sim.allvars.BBNTS.data; bbneut = mean(bbneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 1) = bbneut;', ts, ts));
        btneut = sim.allvars.BTNTS.data; btneut = mean(btneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 2) = btneut;', ts, ts));
        ttneut = sim.allvars.NEUTX.data; ttneut = mean(ttneut(ind));
        eval(sprintf('data_source.Y.%s(ind_shot_plot_%s, 3) = ttneut;', ts, ts));
        
        eval(sprintf('data_source.label.%s{ind_shot_plot_%s} = ''%s'';', ts, ts, ss));
        eval(sprintf('ind_shot_plot_%s = ind_shot_plot_%s+1;', ts,ts));
    end
    
    %% ordering
    order = {'H', 'D', 'HH', 'DD', 'DH', 'HD', 'H@50', 'D@50'};
    index_ordered_H = zeros(1,6);
    index_ordered_D = zeros(1,6);
    for i=1:numel(order)
        for j=1:numel(data_source.label.H)
            if strcmp(order{i}, data_source.label.H(j))
                index_ordered_H(i)=j;
            end
            if strcmp(order{i}, data_source.label.D(j))
                index_ordered_D(i)=j;
            end
        end
    end
    data_source.Y.H = data_source.Y.H(index_ordered_H,:);
    data_source.label.H = data_source.label.H(index_ordered_H);
    data_source.Y.D = data_source.Y.D(index_ordered_D,:);
    data_source.label.D = data_source.label.D(index_ordered_D);    
    %% fancy graphs
    for species={'H', 'D'}
        figure();
        bar(data_source.Y.(species{1}), 'stacked');
        hold('off'); box('on'); grid('on'); legend({'Beam-Beam', 'Beam-Target', 'Target-Target'}, 'location', 'best');
        xticklabels(data_source.label.(species{1})); ylabel(['Neutrons (1/s)'])
        xtickangle(45)
        ylim([0e11, 1e14])
        title(sprintf('%d %s plasma', shotnum, species{1}));
%         set(gca, 'YScale', 'log')
        savefig(gcf, sprintf('%s%d/%d_%starget_source', dir_simulations, shotnum, shotnum, species{1}));    
    end
end

%% plot fast ion densities
if plot_densities
    %% initialize figures
    fh=figure(); haxH=axes; hold(haxH, 'on');
    fd=figure(); haxD=axes; hold(haxD, 'on');
    times=0.9;
    
    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        [~, ind] = min(abs(time-times));

        n_fast = sim.allvars.BDENS.data(:, ind);
        n_e = sim.allvars.NE.data(:, ind);
        n_i = sim.allvars.NI.data(1,:);
        if target_1beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-';
        else
            lc='b-';
        end
        plot(hax, sim.coords.TIME3.data, n_i, lc, 'DisplayName', sprintf('%s', source_1beam{id}));
    end

    %% 2 beams
    for id=1:numel(ids_2beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_2beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        [~, ind] = min(abs(time-times));

        n_fast = sim.allvars.BDENS.data(:, ind);
        n_e = sim.allvars.NE.data(:, ind);
        n_i = sim.allvars.NI.data(1,:);
        if target_2beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_2beam{id}(1) == 'D'
            lc='r';
        else
            lc='b';
        end
        if source_2beam{id}(2)=='H'
            lc = [lc, ':'];
        else
            lc = [lc, '--'];
        end
        plot(hax,sim.coords.TIME3.data, n_i, lc, 'DisplayName', sprintf('%s', source_2beam{id}));
    end

    %% fancy graphs
    for h=[haxH, haxD]
        hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
        xlabel(h, ['time (s)']); ylabel(h, ['n_{i}(0) m^{-3}'])
%         set(h, 'Yscale','log');
    end
    title(haxH, sprintf('%d H plasma', shotnum))
    title(haxD, sprintf('%d D plasma', shotnum))
    savefig(fh, sprintf('%s%d/%d_Hdensities', dir_simulations, shotnum, shotnum))
    savefig(fd, sprintf('%s%d/%d_Ddensities', dir_simulations, shotnum, shotnum))
end

%% comparison between the two beams turned on one-by-one
if plot_comparison_singlebeams
    %% initialize figures
    fh=figure(); haxH=axes; hold(haxH, 'on');
    fd=figure(); haxD=axes; hold(haxD, 'on');

    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        neut = sim.allvars.NEUTT.data;
        if target_1beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-';
        else
            lc='b-';
        end
        semilogy(hax, time, neut, lc, 'DisplayName', sprintf('%s', source_1beam{id}));
    end
    %% 1 beam @ 50
    for id=1:numel(ids_1beam50)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam50{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        neut = sim.allvars.NEUTT.data;
        if target_1beam50{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if strcmp(source_1beam50{id},'D@50')
            lc='r-.';
        else
            lc='b-.';
        end
        semilogy(hax, time, neut, lc, 'DisplayName', sprintf('%s', source_1beam50{id}));
    end
    
    %% fancy graphs
    for h=[haxH, haxD]
        hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
        xlabel(h, ['time (s)']); ylabel(h, ['Neutrons (1/s)'])
        set(h, 'Yscale','log');
    end
    title(haxH, sprintf('%d H plasma', shotnum))
    title(haxD, sprintf('%d D plasma', shotnum))
    savefig(fh, sprintf('%s%d/%d_Htarget_singlebeams', dir_simulations, shotnum, shotnum))
    savefig(fd, sprintf('%s%d/%d_Dtarget_singlebeams', dir_simulations, shotnum, shotnum))
end

%% plot initial values
if plot_initialconditions
    %% initialize figures
    f=figure(); hax=axes; hold(hax, 'on');
    fT=figure(); haxT=axes; hold(haxT, 'on');

    fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{1});
    sim = cdf2mat(fname);
    rho = sim.coords.X.data(:,1);
    ne = sim.allvars.NE.data(:,1)*1e6;
    plot(hax, rho, ne, 'k');

    te = sim.allvars.TE.data(:,1);
    ti = sim.allvars.TI.data(:,1);
    plot(haxT, rho, te, 'k', 'DisplayName', 'e');
    plot(haxT, rho, ti, 'r', 'DisplayName', 'i');
    
    %% fancy graphs
    h=hax;
    hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
    xlabel(h, ['\rho_{TOR}']); ylabel(h, ['n_e (m^{-3})'])
    title(hax, sprintf('%d ', shotnum))
    h=haxT;
    hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
    xlabel(h, ['\rho_{TOR}']); ylabel(h, ['Temperature (eV)'])
    title(haxT, sprintf('%d ', shotnum))
        
    savefig(f, sprintf('%s%d/%d_initial_n', dir_simulations, shotnum, shotnum))
    savefig(fT, sprintf('%s%d/%d_initial_T', dir_simulations, shotnum, shotnum))

end

if convertfig
    cd(sprintf('%s/%d/',dir_simulations, shotnum))
    d=dir('*.fig'); % capture everything in the directory with FIG extension
    allNames={d.name}; % extract names of all FIG-files
    close all; % close any open figures
    for i=1:length(allNames)
          open(allNames{i}); % open the FIG-file
          base=strtok(allNames{i},'.'); % chop off the extension (".fig")
          print('-dpng', base); % export to JPEG as usual
          close(gcf); % close it ("gcf" returns the handle to the current figure)
    end
end

if plot_beta
    %% initialize figures
    fh=figure(); haxH=axes; hold(haxH, 'on');
    fd=figure(); haxD=axes; hold(haxD, 'on');
    times=0.9;
    
    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        [~, ind] = min(abs(time-times));

        beta = sim.allvars.BTPL.data+sim.allvars.BTBE.data;
        % this beta is a radial profile, need to take the volume-average
        vol=sim.allvars.DVOL.data;
        voltot = sum(vol,1);
        beta=dot(beta,vol)./voltot;
        
        %betaN
        Ip = sim.allvars.PCUR.data*1e-6;
        a=sim.allvars.RBOUN.data(end,:)*1e-2;a=a';
        B0 = sim.allvars.BZXR.data/88;
        Ipnorm=Ip./(a.*B0);Ipnorm=Ipnorm';
        beta = beta./Ipnorm;

        if target_1beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-';
        else
            lc='b-';
        end
        plot(hax, sim.coords.TIME3.data,  beta*100., lc, 'DisplayName', sprintf('%s', source_1beam{id}));
    end

    %% 2 beams
    for id=1:numel(ids_2beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_2beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        [~, ind] = min(abs(time-times));

        beta = sim.allvars.BTPL.data+sim.allvars.BTBE.data;
        % this beta is a radial profile, need to take the volume-average
        vol=sim.allvars.DVOL.data;
        voltot = sum(vol,1);
        beta=dot(beta,vol)./voltot;
        %betaN
        Ip = sim.allvars.PCUR.data*1e-6;
        a=sim.allvars.RBOUN.data(end,:)*1e-2;a=a';
        B0 = sim.allvars.BZXR.data/88;
        Ipnorm=Ip./(a.*B0);Ipnorm=Ipnorm';
        beta = beta./Ipnorm;
        
        if target_2beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_2beam{id}(1) == 'D'
            lc='r';
        else
            lc='b';
        end
        if source_2beam{id}(2)=='H'
            lc = [lc, ':'];
        else
            lc = [lc, '--'];
        end
        plot(hax, sim.coords.TIME3.data,  beta*100., lc, 'DisplayName', sprintf('%s', source_2beam{id}));
    end
    %% 1 beam
    for id=1:numel(ids_1beam50)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam50{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        [~, ind] = min(abs(time-times));

        beta = sim.allvars.BTPL.data+sim.allvars.BTBE.data;
        % this beta is a radial profile, need to take the volume-average
        vol=sim.allvars.DVOL.data;
        voltot = sum(vol,1);
        beta=dot(beta,vol)./voltot;
        
        %betaN
        Ip = sim.allvars.PCUR.data*1e-6;
        a=sim.allvars.RBOUN.data(end,:)*1e-2;a=a';
        B0 = sim.allvars.BZXR.data/88;
        Ipnorm=Ip./(a.*B0);Ipnorm=Ipnorm';
        beta = beta./Ipnorm;
        
        if target_1beam50{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam50{id} == 'D@50'
            lc='r-.';
        else
            lc='b-.';
        end
        plot(hax, sim.coords.TIME3.data, beta*100., lc, 'DisplayName', sprintf('%s', source_1beam50{id}));
    end
    %% fancy graphs
    for h=[haxH, haxD]
        hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
        xlabel(h, ['time (s)']); ylabel(h, ['\beta_{N}'])
%         set(h, 'Yscale','log');
    end
    title(haxH, sprintf('%d H plasma', shotnum))
    title(haxD, sprintf('%d D plasma', shotnum))
    savefig(fh, sprintf('%s%d/%d_H_beta', dir_simulations, shotnum, shotnum))
    savefig(fd, sprintf('%s%d/%d_D_beta', dir_simulations, shotnum, shotnum))

end


if plot_losses
    %% initialize figures
    fh=figure(); haxH=axes; hold(haxH, 'on');
    fd=figure(); haxD=axes; hold(haxD, 'on');
    times=0.9;
    
    %% 1 beam
    for id=1:numel(ids_1beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam{id});
        sim = cdf2mat(fname);
        n = nubeam_get(sim, times);
        if target_1beam{id}=='D'
            
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-';
        else
            lc='b-';
        end
        plot(hax, sim.coords.TIME3.data, (n.d1.p_OL+n.d1.p_ST+n.d1.p_CX)./n.d1.p_tot_in*100., lc, 'DisplayName', sprintf('%s', source_1beam{id}));
    end

    %% 2 beams
    for id=1:numel(ids_2beam)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_2beam{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        n = nubeam_get(sim, times);

        if target_2beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_2beam{id}(1) == 'D'
            lc='r';
        else
            lc='b';
        end
        if source_2beam{id}(2)=='H'
            lc = [lc, ':'];
        else
            lc = [lc, '--'];
        end
        plot(hax, sim.coords.TIME3.data, (n.d1.p_OL+n.d1.p_ST+n.d1.p_CX)./n.d1.p_tot_in*100., lc, 'DisplayName', sprintf('%s', source_2beam{id}));

    end
    %% 1 beam@50
    for id=1:numel(ids_1beam50)
        fname = sprintf('%s%d/%d%s.CDF', dir_simulations, shotnum, shotnum, ids_1beam50{id});
        sim = cdf2mat(fname);
        time = sim.coords.TIME3.data;
        n = nubeam_get(sim, times);
        if target_1beam{id}=='D'
            hax = haxD;
        else
            hax=haxH;
        end
        if source_1beam{id} == 'D'
            lc='r-.';
        else
            lc='b-.';
        end
        plot(hax, sim.coords.TIME3.data, (n.d1.p_OL+n.d1.p_ST+n.d1.p_CX)./n.d1.p_tot_in*100., lc, 'DisplayName', sprintf('%s', source_1beam50{id}));
    end
    %% fancy graphs
    for h=[haxH, haxD]
        hold(h,'off'); box(h, 'on'); grid(h, 'on'); legend(h, 'show');
        xlabel(h, ['time (s)']); ylabel(h, ['Power fraction lost (%)'])
        xlim(h, [0.85 0.95])
%         set(h, 'Yscale','log');
    end
    title(haxH, sprintf('%d H plasma', shotnum))
    title(haxD, sprintf('%d D plasma', shotnum))
    savefig(fh, sprintf('%s%d/%d_H_plost', dir_simulations, shotnum, shotnum))
    savefig(fd, sprintf('%s%d/%d_D_plost', dir_simulations, shotnum, shotnum))

end